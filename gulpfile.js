var gulp = require('gulp'),
		uglify = require('gulp-uglify'),
		concat = require('gulp-concat'),
		browserSync = require('browser-sync').create(),
		sass = require('gulp-sass'),
		htmlmin = require('gulp-htmlmin'),
		imagemin = require('gulp-imagemin'),
		cleanCSS = require('gulp-clean-css'),
		webpack = require('webpack-stream');

gulp.task('sass', function() {
	return gulp.src('src/assets/css/main.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(gulp.dest('dist/assets/css'))
		.pipe(browserSync.reload({
			stream: true
		}));
});

gulp.task('htmlmin', function(){
	return gulp.src('src/*.html')
		//.pipe(htmlmin())
		.pipe(gulp.dest('dist/'))
		.pipe(browserSync.reload({
			stream: true
		}));
});

gulp.task('imagemin', function() {
	return gulp.src('src/assets/images/**/*')
		.pipe(imagemin())
		.pipe(gulp.dest('dist/assets/images'));
});

gulp.task('browser-sync', function() {
	browserSync.init({
		server: {
			baseDir: 'dist'
		}
	});

});

gulp.task('webpack', function() {
  return gulp.src('src/assets/js/app.js')
    .pipe(webpack({
      entry: {
        app: './src/assets/js/app.js',
      },
      output: {
        filename: 'all.js',
      },
    }))
    .on('error', function handleError() {
      this.emit('end'); // Recover from errors
    })
    .pipe(gulp.dest('dist/assets/js'));
});

gulp.task('watch', ['htmlmin', 'sass', 'webpack', 'imagemin','browser-sync'], function() {
	gulp.watch('src/assets/css/*.scss', ['sass']);
	gulp.watch('src/assets/js/**/*.js', ['webpack']);
	gulp.watch('src/*.html', ['htmlmin']);
	gulp.watch('src/assets/images/*', ['imagemin']);
});