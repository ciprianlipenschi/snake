/**
  * Barriers...
  */

function Barriers(settings, dispatcher) {
  this.margin = settings.margin;
  this.snake = settings.snake;
  this.dispatcher = dispatcher;
};

Barriers.prototype.snakeCheck = function snakeCheckF(position) {
  var self = this;

  position.position.some(function(el, ind) {
    if(ind === (position.position.length - 1)) {
     return;
    }

    if(el[0] === position.head[0] && el[1] === position.head[1]) {
      console.log('in snake touch');
      self.dispatcher.publish({
        event: 'snake_collision',
        payload: {}
      });
      console.log('SNAKE TOUCH');
      return true;
    }
  })
};

Barriers.prototype.margins = function marginsF() {};

Barriers.prototype.receive = function receiveF(event) {
  if(this.snake) {
    this.snakeCheck(event.payload);
  }

  if(this.margin) {
    this.margin(event.payload);
  }
};

export default Barriers;
