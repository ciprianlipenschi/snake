/*
  ==========
  FRUIT
  ==========
*/

/*
  Imports:
  ==========
*/
import { getRandomNoCeil, equalArrays } from '../utils/utils.js';
/*
  TODOs:
  - refactor fruit methods
*/

function Fruit(randomNoFn, tableSize, dispatcher) {
	this.position = [randomNoFn(tableSize), randomNoFn(tableSize)];
  this.dispatcher = dispatcher;
};

Fruit.prototype.setCoordinates = function setCoordinatesF(randomNoFn, tableSize) {
  this.position[0] = randomNoFn(tableSize);
  this.position[1] = randomNoFn(tableSize);
  this.renderFruit()
};

Fruit.prototype.renderFruit = function renderFruitF() {
	$('[data-position="x-' + this.position[0] + '|y-' + this.position[1] + '"]').addClass('Fruit');
};

Fruit.prototype.removeFruit = function removeFruitF(test) {
  console.log('in removeFruit - logginf "this"');
  console.log(this)

  if(equalArrays(test, this.position)) {
	 $('.Fruit').removeClass('Fruit');
   console.log('fruit removed')
   this.setCoordinates(getRandomNoCeil, 20);
   console.log('fruit generated')
   this.dispatcher.publish({'event':'fruit_eaten', 'payload': ''})
  }
};

Fruit.prototype.receive = function receiveF(event) {
  switch(event.event) {
    case 'snake_move':
      console.log('fruit - received event')
      this.removeFruit(event.payload.head);
      break;
    case 'key_space':
      this.renderFruit();
      break;
  }
};

export default Fruit;
