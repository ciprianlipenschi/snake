/*
  ==========
  SCORE
  ==========
*/

/*
  Imports:
  ==========
*/
//import view from '../view/view.js';

/*
  TODOs:
  
*/

export default function Score(initialScore, dispatcher) {
  this.score = initialScore;
  this.dispatcher = dispatcher;
};

Score.prototype.receive = function receiveF(event) {
  switch(event.event) {
    case 'fruit_eaten':
      this.score += 7;
      //view.renderScoreBoard(this.score)
      this.dispatcher.publish({
        event: 'score_update',
        payload: {
          score: this.score
        }
      });
  }
};
