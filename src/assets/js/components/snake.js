/*
  ==========
  SNAKE
  ==========
*/

/*
  Imports:
  ==========
*/
import view from '../view/view.js';
import { addPubSub } from '../utils/utils.js';
/*
  TODOs:
  - add a 'get settings' functionality;
*/

function Snake(position, dispatcher, store) {
  this.moving = false;
  this.live = false;
  this.position = position || [[2,2], [3,2], [4,2]];
  this.length = this.position.length;
  this.dispatcher = dispatcher;
  // get initial state
  this.directions = store.getState().directions;
};

Snake.prototype.move = function moveF(dir) {
  // var currentHead - current snake head position;
  var currentHead = JSON.parse(JSON.stringify(this.position[this.position.length - 1]));
  // var director - X || Y coordinate
  // if (0) => dir === 'left' || dir === 'right' - X coordinate - first currentHead element;
  // if (1) => dir === 'up' || dir === 'down' - Y coordinate - second currentHead element;
  var director = 0;
  // var acumulator - increment || decrement X || Y value for next move;
  // if (-1) => dir === 'left' || dir === 'up';
  // if (1) => dir === 'right' || dir === 'down';
  var acumulator = -1; 
  // var nextHead - next snake head position;
  var nextHead = [];
  // var updatedVector - next value for direction vector;
  var updatedVector;


  // Set new director, if dir (direction) is different from 'left' || 'right';
  if(dir === 'up' || dir === 'down') {
    director = 1;
  }
  // Set new acumulator value, if dir (direction) is different from 'left' || 'up';
  if(dir === 'right' || dir === 'down') {
    acumulator = 1;
  }
  // Compute value of updated vector and define updatedVector variable;
  updatedVector = currentHead[director] + acumulator;

  // If updatedVector is outside table bounds, change the updatedVector variable to a value
  // that is within the table bounds;
  if(updatedVector < 1) {
    updatedVector = 20;
  }

  if(updatedVector > 20) {
    updatedVector = 1;
  }
  // Define the nextHead variable with a new value based on the updatedVector;
  nextHead[director] = updatedVector;
  nextHead[Number(!director)] = currentHead[Number(!director)];

  // Add a new head position value based on the nextHead variable to the snake position array 
  // and remove the tail of 
  this.position.push(nextHead);

  console.log('this.length')
  console.log(this.length)
  console.log('this.position.length')
  console.log(this.position.length)


  if (this.length !== this.position.length) {
    this.position.shift(); 
  }

  console.log('this')
  console.log(this)
  // Broadcast snake position;
  this.dispatcher.publish({
    event: 'snake_move',
    payload: { 
      head: this.position[this.position.length - 1],
      position: this.position
    }
  });
  console.log('snake - dispatched head position && position');

  // Render snake;
  //view.renderSnake(this);
};

// Add pub/sub props to the Snake constructor
// addPubSub(Snake);

Snake.prototype.receive = function receiveF(event) {
  switch(event.event) {
    case 'fruit_eaten':
      this.length += 1;
      break;
    case 'snake_collision':
      this.stop();
      break;
    case 'key_space':
      this.start();
      break;
    case 'key_p':
      if(this.moving === true) {
        //this.moving = false;
        this.stop();
      } else {
        //this.moving = true;
        this.start();
      }
      break;
    case 'state_update':
      this.directions = event.payload.directions;
  }
};

/**
 * Starts the snake's movement
 *
 * Start the snake's movement by calling setInterval with the 'move' function.
 *
 * @param {Object} directions - movement directions
 * @param {number} delay - at what interval the 'move' function is being run.
 * @returns {void}
 */

Snake.prototype.start = function startF(directions, delay) {
  directions = directions || {
    current: 'right',
    opposing: 'left'
  };
  delay = delay || 200;
  var self = this;

  if(!this.moving) {
    this.moving = true;
    this.live = setInterval(function() {self.move(self.directions.current)}, delay);
  }
};

/**
 * Stop the snake's movement
 *
 * Stop the snake's movement by clearing the 'start' prop.
 *
 * @returns {void}
 */

Snake.prototype.stop = function stopF() {
  console.log('in stop');
  this.moving = false;
  clearInterval(this.live);
};

export default Snake;
