/*
  ==========
  CONTROL
  ==========
*/

/*
  Imports:
  ==========
*/
import { getRandomNoCeil, equalArrays } from '../utils/utils.js';
import NotifCenter from '../utils/pubSub.js';
import View from '../view/view.js';
import Snake from '../components/snake.js';
import Fruit from '../components/fruit.js';
import Score from '../components/score.js';
import Barriers from '../components/barriers.js';
import Store from '../data/data.js';

/*
  TODOs:
  - (none so far...);
  - extend dispatcher to allow for multiple event without separate subscription declarations; 
*/



var control = (function control() {
 // var moving = false; // Checks wether snake is moving;
 // var start; // Holds Timeout fn for snake movement;
  var snake; // Holds snake;
  var fruit; // Holds fruit;
  var barriers; // Holds barriers;
  var score; // Holds score;
  var notifCenter;
  var view;
  var store;


  var ctrl = {

    init: function initF() {
      notifCenter = new NotifCenter();
      store = new Store(notifCenter);
      view = new View('', notifCenter);
      snake = new Snake([[2,2], [3,2], [4,2]], notifCenter, store);
      fruit = new Fruit(getRandomNoCeil, 20, notifCenter);
      score = new Score(0, notifCenter);
      barriers = new Barriers({snake: true, margin: false}, notifCenter);

      console.log('view -- ctr init')

      view.init();


      fruit.dispatcher.subscribeToEvent('snake_move', fruit.receive.bind(fruit));
      barriers.dispatcher.subscribeToEvent('snake_move', barriers.receive.bind(barriers));
      snake.dispatcher.subscribeToEvent('fruit_eaten', snake.receive.bind(snake));
      snake.dispatcher.subscribeToEvent('snake_collision', snake.receive.bind(snake));
      snake.dispatcher.subscribeToEvent('key_space', snake.receive.bind(snake));
      fruit.dispatcher.subscribeToEvent('key_space', fruit.receive.bind(fruit));
      snake.dispatcher.subscribeToEvent('key_p', snake.receive.bind(snake));
      snake.dispatcher.subscribeToEvent('state_update', snake.receive.bind(snake));
      score.dispatcher.subscribeToEvent('fruit_eaten', score.receive.bind(score));
      view.dispatcher.subscribeToEvent('snake_move', view.receive.bind(view));
      view.dispatcher.subscribeToEvent('score_update', view.receive.bind(view));
      store.dispatcher.subscribeToEvent('directions_update', store.receive.bind(store))


      console.log('snake')
      console.log(snake)
      console.log('fruit')
      console.log(fruit)
      console.log('score')
      console.log(score)
    }
  };

  return {
    init: ctrl.init,
    userInput: ctrl.userInput
  }
})();

export default control;
