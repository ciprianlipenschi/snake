/**
 * Middleware for various data transformation
 *
 */

userInput: function userInput(input) {
  console.log('in user input')
  console.log(input)
  switch(input) {
    case 32: // 'space' key
      if (moving === false) {
        console.log('in space key')

        view.renderSnake(snake);
        fruit.renderFruit()
        view.renderScoreBoard(0)
        console.log('directions.current')
        console.log(directions.current)
        console.log('===== separator =====')
        start = setInterval(function() {snake.move(directions.current)}, 200);
        console.log('start')
        console.log(start)
        moving = true;
        console.log('moving')
        console.log(moving)
      }
      break;
    case 37: // left arrow
      ctrl.setDirections('left', 'right');
      break;
    case 38: // up arrow
      ctrl.setDirections('up', 'down');
      break;
    case 39: // right arrow
      ctrl.setDirections('right', 'left');
      break;
    case 40: // down arrow
      ctrl.setDirections('down', 'up');
      break;
    case 80: // 'p' key
      console.log('moving')
      console.log(moving)
      if(moving) {
        window.clearInterval(start);
        moving = false;
        return;
      }
      start = setInterval(function() {snake.move(directions.current)}, 200);
      moving = true;
      break;
  }
}