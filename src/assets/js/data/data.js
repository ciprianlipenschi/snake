
function Store(dispatcher) {
  this.dispatcher = dispatcher;
  this.state = {
    directions: {
      current: 'right',
      opposing: 'left'
    }
  };
}


Store.prototype.getState = function getStateF() {
  //console.log(state)
  return this.state;
};

Store.prototype.setState = function setState(newState) {

  if(arguments[0] !== undefined) {
    if (Object.prototype.toString.call(newState) !== '[object Object]')
      throw 'seState fn `state` argument is not an object. Pass an object as the `state` argument';
  }

  this.state = $.extend(true, {}, this.state, newState);
  //console.log(state);
  this.dispatcher.publish({
    event: 'state_update',
    payload: this.state
  })
};

Store.prototype.receive = function receiveF(event) {
  switch(event.event) {
    case 'directions_update':
      this.setState(event.payload);
      break;
  }
};

export default Store;
