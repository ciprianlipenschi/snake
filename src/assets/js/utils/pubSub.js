/*
  ==========
  PUB-SUB
  ==========
*/

// Basic constructor fn. Creates an event list that takes
// actions as property names and each prop takes an array
// as a list of subscriber functions.
export default function NotifCenter() {
	this.eventsList = {};
};

// Subscribe to events fn.
NotifCenter.prototype.subscribeToEvent = function subscribeToEventF(event, subscriber) {
	// Creates the events list for the specified event if it doesn't already exists.
	this.eventsList[event] = this.eventsList[event] || [];

	try {
		// Check wether subscriber argument is a function and throws an error if not.
		if (typeof subscriber !== 'function') throw 'Subscriber is not a function!';
		// Register the subscriber to the event list.
		this.eventsList[event].push(subscriber);
	} catch(err) {
		console.log(err);
	};

};

NotifCenter.prototype.unsubscribeFromEvent = function unsubscribeFromEventF(event, subscriber) {
	this.eventsList[event] = this.eventsList[event].filter(function(elem) {
		return elem != subscriber;
	});
};

NotifCenter.prototype.printSubscribers = function printSubscribersF() {
	for (x in this.eventsList) {
		this.eventsList[x].forEach(function(el) {
			console.log(el.id)
		})
	}
};

NotifCenter.prototype.publish = function publishF(event) {
	if(!this.eventsList.hasOwnProperty(event.event)) {
		return;
	}

	this.eventsList[event.event].forEach(function(el) {
		el(event);
	});
	console.log('dispatcher - dispatched event')
	console.log(event.event)
};

// tests...

var abc = new NotifCenter();

var bce = {
	smth: function smthF() {}
}

abc.subscribeToEvent('a', bce.smth.bind(bce))

console.log(abc)