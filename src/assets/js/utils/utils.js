export function getRandomNoCeil(val) {
	return Math.ceil(Math.random() * val);
};

/*var result = [];
var i = 0;

while(i < 100000) {
	result.push(getRandomNoCeil(20));
	i++;
};



console.log(Math.max.apply(null, result))
console.log(Math.min.apply(null, result))*/

export function equalArrays(arr1, arr2) {
	var a = arr1.length;

	if (arr1.length !== arr2.length) {
		return false;
	}

	while(a--) {
		if(arr1[a] !== arr2[a]) {
			return false;
		}
	}

	return true;
};

//console.log(equalArrays([1,2,3], [1,2,3]))

/* Can add pub-sub properties to a constructor */
export function addPubSub(target) {
	target.prototype.addSubscribers = function addSubscribersF(elem) {
		this.subscribers = this.subscribers || [];
		this.subscribers.push(elem);
	};

	target.prototype.removeSubscribers = function removeSubscribersF(elem) {
		if(!this.subscribers) return;
		this.subscribers = this.subscribers.filter(function(el) {
			return el !== elem;
		});
	};

	target.prototype.publish = function publishF(payload) {
		this.subscribers.forEach(function(el) {
			if(!(typeof el === 'function')) return;
			el(payload);
		})
	};
};
