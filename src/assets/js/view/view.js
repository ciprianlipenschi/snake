/*
  ==========
  VIEW
  ==========
*/

/*
  Imports:
  ==========
*/
import control from '../control/control.js';

/*
  TODOs:
  - add a 'get settings' functionality;
*/

function View(viewElements, dispatcher) {
  this.viewElements = viewElements || {
    body: 'body',
    board: '.Board',
    boardSquare: 'Board-square',
    boardSquareA: '.Board-square', // dafuq... Y tho?
    snakePiece: '.Snake-piece',
  };
  this.dispatcher = dispatcher;
  this.directions = {
    current: 'right',
    opposing: 'left',
  };
};


View.prototype.generateTable = function generateTableF() {
  for(var i, i = 1; i < 21; i++) {
    for(var j, j = 1; j < 21; j++) {
      var a = $('<div/>', {class: this.viewElements.boardSquare}).attr('data-position', ('x-' + j + '|y-' + i));
      a.appendTo(this.viewElements.board);
    }
  }
};

View.prototype.renderSnake = function renderSnakeF(target) {
  $(this.viewElements.boardSquareA).removeClass('Snake-piece');
  target.position.forEach(function(el) {
    console.log(el)
    $('[data-position="x-' + el[0] + '|y-' + el[1] + '"]').addClass('Snake-piece');
  })
};

View.prototype.renderScoreBoard = function renderScoreBoardF(score) {
  if(!$('.Score').length) {
    var el = $('<p/>', {class: 'Score', text: score});
    $('.u-innerContainer').prepend(el);
  }

  $('.Score').text(score);
};

View.prototype.bindEvents = function bindEvents() {
  var self = this;
  document.addEventListener('keydown', function(event) {
    self.userInput(event.which);
  })
};

View.prototype.init = function initF() {
  this.generateTable();
  this.bindEvents();
};

View.prototype.receive = function receiveF(event) {
  switch(event.event) {
    case 'snake_move':
      this.renderSnake(event.payload);
      break;
    case 'score_update':
      this.renderScoreBoard(event.payload.score);
      break;
  }
};

/**
 * Transform keyboard input into movement directions
 *
 */

View.prototype.userInput = function userInputF(input) {
  console.log('in user input')
  console.log(input)
  switch(input) {
    case 32: // 'space' key
      this.dispatcher.publish({
        event: 'key_space',
        payload: {}
      });
      break;
    case 37: // left arrow
      this.setDirections('left', 'right');
      break;
    case 38: // up arrow
      this.setDirections('up', 'down');
      break;
    case 39: // right arrow
      this.setDirections('right', 'left');
      break;
    case 40: // down arrow
      this.setDirections('down', 'up');
      break;
    case 80: // 'p' key
      this.dispatcher.publish({
        event: 'key_p',
        payload: {}
      });
      break;
  }
};

/**
 * Set the movement direction && sends a notification
 *
 */

View.prototype.setDirections = function setDirectionsF(a, b) {
  if (this.directions.current === a || this.directions.opposing === a) {
    return;
  }

  this.directions.current = a;
  this.directions.opposing = b;
  console.log('directions');
  console.log(this.directions);

  this.dispatcher.publish({
    event: 'directions_update',
    payload: {
      directions: this.directions
    }
  })

};

export default View;
